# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=wireplumber
pkgver=0.4.6
pkgrel=0
pkgdesc="Session / policy manager implementation for PipeWire"
url="https://pipewire.org/"
# s390x blocked by pipewire
arch="all !s390x"
license="LGPL-2.1-or-later"
depends_dev="
	elogind-dev
	glib-dev
	lua5.4-dev
	pipewire-dev>=0.3.39
	"
makedepends="$depends_dev
	doxygen
	graphviz
	meson
	"
checkdepends="
	dbus
	pipewire
	"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-logind"
source="https://gitlab.freedesktop.org/PipeWire/wireplumber/-/archive/$pkgver/wireplumber-$pkgver.tar.gz"

case "$CARCH" in
	armhf|armv7) options="$options !check" # Test failures https://gitlab.freedesktop.org/pipewire/wireplumber/-/issues/81
esac

provides="pipewire-session-manager"
provider_priority=0

build() {
	abuild-meson \
		-Dsystem-lua=true \
		-Delogind=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

logind() {
	# This not only needs libelogind, but also elogind being installed and
	# running. Otherwise wireplumber fails to launch.
	depends="$depends elogind"
	# The purpose of the (e)logind module currently is limited to preventing
	# GDM from hijacking the Bluetooth HFP/HSP profile. But GDM users cannot
	# use bluetooth audio without, so install it for them
	install_if="$pkgname=$pkgver-r$pkgrel gdm"
	pkgdesc="wireplumbers optional (e)logind integration needed by GDM users"

	amove usr/lib/wireplumber-0.4/libwireplumber-module-logind.so
}

sha512sums="
4b4b9aff6e0e6d7c567e20e4df533cfd16287f2e7498ae8533a9a4251066e6d0a9cd99e3da48f525bb2010053f7c9918fe09a5ade39c8830ec08c24292527684  wireplumber-0.4.6.tar.gz
"
