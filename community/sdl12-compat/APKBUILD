# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=sdl12-compat
# Have the same version as the last upstream release
pkgver=0.0.1_git20211216
_commit=d05899f95c5d7e7e2d8eee40ddf3815e53a3bddf
pkgrel=0
pkgdesc="SDL-1.2 compat layer that uses SDL-2.0"
options="!check" # no testsuite
url="https://github.com/libsdl-org/sdl12-compat"
arch="all"
license="BSD-3-Clause"
depends="sdl2"
makedepends="cmake sdl2-dev"
subpackages="$pkgname-dev"
source="$pkgname-$_commit.tar.gz::https://github.com/libsdl-org/sdl12-compat/archive/$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"

# Otherwise the package will fail installation if sdl is already present
# NOTE(Leo): remove this before the release of 3.15
replaces="sdl"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSDL12TESTS=OFF \
		-DCMAKE_C_FLAGS="$CFLAGS -I/usr/include/directfb" \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# Claim the provider 'pc:sdl=$pkgver' by creating a symlink
	# so packages can find it
	ln -s sdl12_compat.pc "$pkgdir"/usr/lib/pkgconfig/sdl.pc
}

dev() {
	# Otherwise the package will fail installation if sdl is already present
	# NOTE(Leo): remove this before the release of 3.15
	replaces="sdl-dev"

	default_dev
}

sha512sums="
0aec335246a59160f04bed798fa36f9f39618e9c8a05f08c95f9153f7e0910cfc808e4ba3ce19ce9ff2035201490a78053eca72fb282d76a1b377fd0042906ed  sdl12-compat-d05899f95c5d7e7e2d8eee40ddf3815e53a3bddf.tar.gz
"
