# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dns-lexicon
pkgver=3.9.0
pkgrel=0
pkgdesc="Manipulate DNS records on various DNS providers in a standardized/agnostic way"
url="https://github.com/AnalogJ/lexicon"
arch="noarch"
license="MIT"
depends="
	py3-tldextract
	py3-requests
	py3-yaml
	py3-future
	py3-cryptography
	py3-beautifulsoup4
	"
makedepends="py3-setuptools"
_providerdepends="
	py3-boto3
	py3-xmltodict
	py3-localzone
	py3-softlayer
	py3-zeep
	"
checkdepends="
	py3-filelock
	py3-pytest
	py3-pytest-mock
	py3-requests-file
	py3-vcrpy
	$_providerdepends
	"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/d/dns-lexicon/dns-lexicon-$pkgver.tar.gz
	https://github.com/AnalogJ/lexicon/archive/refs/tags/v$pkgver.zip"
builddir="$srcdir/dns-lexicon-$pkgver"

prepare() {
	default_prepare
	# copy test data from zip
	cp -a "$srcdir/lexicon-$pkgver/tests" "$srcdir/dns-lexicon-$pkgver/"
	# delete broken test
	rm "$srcdir/dns-lexicon-$pkgver/lexicon/tests/providers/test_oci.py"
}
build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/lexicon/tests
}

sha512sums="
c8b51f8c0fc79ac68d07d644671417d952974cb3a350a92bd167af8ec7806e149ac2dc8c2f30785d995910cbc0cba3150a4434cf9f99fe1458ee5ece8183a4a1  py3-dns-lexicon-3.9.0.tar.gz
5bb6656dd665d71b8557784b5d6eebcbd3b6e5c1a42b2b8f7cbcd8fa57dc17c3739faed2fa11b762426e0cc8ec2275869a7aa73f98f077363df3309d62ed5431  v3.9.0.zip
"
