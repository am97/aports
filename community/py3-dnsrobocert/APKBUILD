# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dnsrobocert
pkgver=3.16.0
pkgrel=0
pkgdesc="A tool to manage your DNS-challenged TLS certificates"
url="https://dnsrobocert.readthedocs.io/en/latest/"
arch="noarch !mips64" # blocked by py3-boto3
license="MIT"
depends="
	certbot
	py3-acme
	py3-boto3
	py3-cffi
	py3-cryptography
	py3-colorama
	py3-coloredlogs
	py3-dnspython
	py3-dns-lexicon
	py3-jsonschema
	py3-localzone
	py3-lxml
	py3-openssl
	py3-pem
	py3-schedule
	py3-softlayer
	py3-tldextract
	py3-xmltodict
	py3-yaml
	py3-zeep
	"
makedepends="pyproject2setuppy"
checkdepends="pebble py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://github.com/adferrand/dnsrobocert/archive/v$pkgver.tar.gz
	pebble.patch
	"
builddir="$srcdir/dnsrobocert-$pkgver"

build() {
	python3 -m pyproject2setuppy.main build
}

check() {
	PYTHONPATH=build/lib pytest
}

package() {
	python3 -m pyproject2setuppy.main install --prefix=/usr --root="$pkgdir"
}

sha512sums="
026bdf42d52c0ab6601719d573d1cdc94e0cbae652baf2a76e2b9b3ee67a95124517623033b6748b48cd166151bf536377a08310137e17dbcb1164e74894505b  py3-dnsrobocert-3.16.0.tar.gz
3a8f2d9a74a35aea2e5eebcede656d2861382c975dc94560eca4f94cd8b13f1bb4a98b5b667cb5937ef9123a8f1da20dcef58a8ffc903e93e979d928bca9f9b1  pebble.patch
"
